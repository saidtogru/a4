public class AdvancedCalculator {

    // Matrix addition
    public double[][] addMatrices(double[][] matrix1, double[][] matrix2) {
        if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
            throw new IllegalArgumentException("Matrices dimensions do not match.");
        }
        double[][] result = new double[matrix1.length][matrix1[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[i].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return result;
    }

    // Standard deviation
    public double standardDeviation(double[] numbers) {
        if (numbers == null || numbers.length == 0) {
            throw new IllegalArgumentException("Array darf nicht leer sein");
        }
        double sum = 0.0, standardDeviation = 0.0;
        int length = numbers.length;

        for(double num : numbers) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: numbers) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }

    // Matrix multiplication
    public double[][] multiplyMatrices(double[][] matrix1, double[][] matrix2) {
        if (matrix1[0].length != matrix2.length) {
            throw new IllegalArgumentException("Matrices dimensions do not match for multiplication.");
        }
        double[][] result = new double[matrix1.length][matrix2[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                for (int k = 0; k < matrix1[0].length; k++) {
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        return result;
    }

    // Linear regression (y = ax + b)
    public double[] linearRegression(double[][] dataPoints) {
        if (dataPoints == null || dataPoints.length < 2) {
            throw new IllegalArgumentException("Mindestens zwei Datenpunkte erforderlich");
        }
        int n = dataPoints.length;
        double sumX = 0, sumY = 0, sumXx = 0, sumXy = 0;

        for (int i = 0; i < n; i++) {
            sumX += dataPoints[i][0];
            sumY += dataPoints[i][1];
            sumXx += dataPoints[i][0] * dataPoints[i][0];
            sumXy += dataPoints[i][0] * dataPoints[i][1];
        }

        double a = (n * sumXy - sumX * sumY) / (n * sumXx - sumX * sumX);
        double b = (sumY - a * sumX) / n;

        return new double[]{a, b}; // Returns coefficients [a, b] for the equation y = ax + b
    }
}
