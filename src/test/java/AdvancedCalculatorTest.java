import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AdvancedCalculatorTest {

    private AdvancedCalculator calc = new AdvancedCalculator();

    // Überprüft die korrekte Ausführung der Addition bei gültigen Matrizen
    @Test
    public void testAddMatrices() {
        double[][] matrix1 = {{1, 2}, {3, 4}};
        double[][] matrix2 = {{5, 6}, {7, 8}};
        double[][] expected = {{6, 8}, {10, 12}};
        assertArrayEquals(expected, calc.addMatrices(matrix1, matrix2));
    }

    // Überprüft, ob eine Ausnahme bei ungleichen Matrixdimensionen ausgelöst wird
    @Test
    public void testAddMatricesWithException() {
        double[][] matrix1 = {{1, 2}};
        double[][] matrix2 = {{3, 4}, {5, 6}};
        assertThrows(IllegalArgumentException.class, () -> {
            calc.addMatrices(matrix1, matrix2);
        });
    }

    // Überprüft die korrekte Berechnung der Standardabweichung für einen Standarddatensatz
    @Test
    public void testStandardDeviation() {
        double[] numbers = {1, 2, 3, 4, 5};
        double expected = 1.4142;
        assertEquals(expected, calc.standardDeviation(numbers), 0.0001);
    }

    // Testet den Fall, wenn das Array nur ein Element enthält
    @Test
    public void testStandardDeviationSingleElement() {
        double[] numbers = {2};
        double expected = 0;
        assertEquals(expected, calc.standardDeviation(numbers), 0.0001);
    }

    // Testet den Ausnahmefall, wenn ein leeres Array übergeben wird
    @Test
    public void testStandardDeviationEmpty() {
        double[] numbers = {};
        assertThrows(IllegalArgumentException.class, () -> {
            calc.standardDeviation(numbers);
        });
    }

    // Überprüft die korrekte Ausführung der Multiplikation bei gültigen Matrizen
    @Test
    public void testMultiplyMatrices() {
        double[][] matrix1 = {{1, 2}, {3, 4}};
        double[][] matrix2 = {{2, 0}, {1, 2}};
        double[][] expected = {{4, 4}, {10, 8}};
        assertArrayEquals(expected, calc.multiplyMatrices(matrix1, matrix2));
    }

    // Überprüft, ob eine Ausnahme bei ungleichen Matrixdimensionen für die Multiplikation ausgelöst wird
    @Test
    public void testMultiplyMatricesWithException() {
        double[][] matrix1 = {{1, 2, 3}};
        double[][] matrix2 = {{4, 5}, {6, 7}};
        assertThrows(IllegalArgumentException.class, () -> {
            calc.multiplyMatrices(matrix1, matrix2);
        });
    }

    // Überprüft die korrekte Berechnung der linearen Regression für einen Standarddatensatz
    @Test
    public void testLinearRegression() {
        double[][] dataPoints = {{1, 2}, {2, 3}, {3, 4}};
        double[] expected = {1, 1}; // Erwartete lineare Gleichung y = x + 1
        assertArrayEquals(expected, calc.linearRegression(dataPoints), 0.0001);
    }

    // Testet den Ausnahmefall, wenn nur ein Datenpunkt vorhanden ist
    @Test
    public void testLinearRegressionSinglePoint() {
        double[][] dataPoints = {{2, 3}};
        assertThrows(IllegalArgumentException.class, () -> {
            calc.linearRegression(dataPoints);
        });
    }
}
