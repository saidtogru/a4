import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SimpleCalculatorTest {

    // Anweisungsüberdeckung (Statement Coverage)
    // Ziel: Sicherstellen, dass jede Anweisung in der Methode mindestens einmal ausgeführt wird.
    @Test
    public void testStatementCoverage() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet die Addition und die Division (a + b, result / 2)
        assertEquals(1, calc.calculate(1, 2, true, false));
    }

    // Zweigüberdeckung (Branch Coverage)
    // Ziel: Sicherstellen, dass jeder Zweig jeder bedingten Anweisung getestet wird.
    @Test
    public void testBranchCoverage1() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet die wahren Zweige der bedingten Anweisungen (a + b, result * 2)
        assertEquals(6, calc.calculate(1, 2, true, true));
    }

    @Test
    public void testBranchCoverage2() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet die falschen Zweige der bedingten Anweisungen (a - b, result / 2)
        assertEquals(0, calc.calculate(1, 2, false, false));
    }

    // Pfadüberdeckung (Path Coverage)
    // Ziel: Sicherstellen, dass jeder mögliche Pfad durch die Methode getestet wird.
    @Test
    public void testPathCoverage1() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet den Pfad, bei dem beide Bedingungen wahr sind (a + b, result * 2)
        assertEquals(6, calc.calculate(1, 2, true, true));
    }

    @Test
    public void testPathCoverage2() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet den Pfad, bei dem die erste Bedingung wahr und die zweite falsch ist (a + b, result / 2)
        assertEquals(1, calc.calculate(1, 2, true, false));
    }

    @Test
    public void testPathCoverage3() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet den Pfad, bei dem die erste Bedingung falsch und die zweite wahr ist (a - b, result * 2)
        assertEquals(-2, calc.calculate(1, 2, false, true));
    }

    @Test
    public void testPathCoverage4() {
        SimpleCalculator calc = new SimpleCalculator();
        // Testet den Pfad, bei dem beide Bedingungen falsch sind (a - b, result / 2)
        assertEquals(0, calc.calculate(1, 2, false, false));
    }
}
