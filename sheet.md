## 4 Software-Testing mit GitLab CI/CD und Docker

### Testfallerstellung für `SimpleCalculator`
Untersuchen Sie die triviale SimpleCalculator-Klasse im Verzeichnis a4/src/main/java und entwickeln Sie JUnit-Tests, um die Unterschiede zwischen Anweisungsüberdeckung, Zweigüberdeckung und Pfadüberdeckung zu demonstrieren. Orientieren Sie sich dabei an den in den Kapiteln 3.2, 3.3 und 3.6 beschriebenen Konzepten. Ihre Hauptaufgabe besteht darin, für jede dieser Testarten spezifische Testfälle zu schreiben und mit Kommentaren zu versehen, um zu illustrieren, wie sich jede Testart von den anderen unterscheidet. Beachten Sie, dass dabei bewusst Redundanzen entstehen, die helfen, die Unterschiede und Gemeinsamkeiten der Testmethoden zu verdeutlichen.

### Testfallerstellung für `AdvancedCalculator`
Untersuchen Sie die AdvancedCalculator-Klasse im Verzeichnis a4/src/main/java. Machen Sie sich gründlich mit den Methoden und deren Funktionsweisen vertraut. Entwickeln Sie in a4/src/test/java JUnit-Tests für die Methoden der AdvancedCalculator-Klasse. Beachten Sie bei der Erstellung Ihrer Tests die Coding-Standards für Unittests, um die Lesbarkeit und Wartbarkeit des Codes zu gewährleisten.

### Ausführung von Tests in Docker
Für diese Aufgabe benötigen Sie lediglich Docker. Stellen Sie sicher, dass Docker auf Ihrem System installiert und betriebsbereit ist. Überprüfen Sie die beigefügte pom.xml-Datei, um sich mit den Maven-Abhängigkeiten Ihres Projekts vertraut zu machen. Verwenden Sie den folgenden Docker-Befehl, um die JUnit-Tests in einer isolierten Umgebung auszuführen:

```
docker run --rm -v "$PWD":/usr/a4 -w /usr/a4 maven:3-openjdk-18 mvn test
```

Stellen Sie sicher, dass alle Tests erfolgreich durchlaufen. Überprüfen Sie die Testergebnisse sorgfältig, um sicherzustellen, dass keine Fehler oder Probleme vorliegen.

### GitLab CI/CD Pipeline
Erstellen Sie ein neues GitLab-Repository für Ihr Projekt, falls noch nicht geschehen. Konfigurieren Sie eine GitLab CI/CD Pipeline in Ihrem Repository. Diese Pipeline soll bei jedem Push automatisch ein Maven Build des Projekts durchführen und alle JUnit-Tests ausführen. Achten Sie darauf, das Maven-Image in Ihrer .gitlab-ci.yml-Konfigurationsdatei zu spezifizieren. Dies stellt sicher, dass Ihre Pipeline die notwendigen Abhängigkeiten für den Build-Prozess und die Testausführung hat.

### Testen der Pipeline
Führen Sie einen Test-Commit in Ihrem GitLab-Repository durch, um die Funktionalität Ihrer CI/CD-Pipeline zu überprüfen. Beobachten Sie den Prozess der Pipeline auf der GitLab-Benutzeroberfläche. Achten Sie darauf, dass jede Phase (Stage) wie erwartet abläuft und keine Fehler auftreten.

---

Ihre Abgabe sollte sowohl den Quellcode der Testklassen als auch die .gitlab-ci.yml-Konfigurationsdatei für die GitLab CI/CD-Pipeline enthalten. Stellen Sie sicher, dass beide Teile korrekt implementiert und dokumentiert sind.

